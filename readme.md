# IPL Data Project I

**In this data assignment we will transform raw data of IPL to calculate the following stats:**

1. Number of matches played per year for all the years in IPL.
2. Number of matches won per team per year in IPL.
3. Extra runs conceded per team in the year 2016
4. Top 10 economical bowlers in the year 2015
5. Find the number of times each team won the toss and also won the match
6. Find a player who has won the highest number of Player of the Match awards for each season

Implemented the functions, one for each task. Used the results of the functions to dump JSON files in the output folder

**Instructions:**

1. Created a new repo with name js-ipl-data-project in Gitlab/Github, before starting implementation of the solution
2. Made sure to follow proper Git practices
3. Before submission, made sure that all the points in the below checklist are covered:
4. Git commits
5. Directory structure
    package.json - dependencies, devDependencies
    .gitignore file
    Proper/Intuitive Variable names
    Separate module for functions
6. Directory structure:
    - src/
    - server/
        - 1-matches-per-year.cjs
        - 2-matches-won-per-team-per-year.cjs
        - ...
    - public/
        - output
            - 1-matches-per-year.json
            - 2-matches-won-per-team-per-year.json
            - ...
    - data/
        - matches.csv
        - deliveries.csv
    - package.json
    - package-lock.json
    - .gitignore
