const csvFilePath='data/matches.csv'
const csv=require('csvtojson')
const fs = require('fs');

csv()
 .fromFile(csvFilePath)
 .then((jsonObj)=>{
     const newJsonObj = jsonObj
     console.log(newJsonObj);
 
     fs.writeFile('public/output/matches.json', JSON.stringify(newJsonObj), (err) => {
        if (err) {
          console.error(err);
          return;
        }
        console.log('JSON file has been saved.');
      });
    })
    .catch((err) => {
      console.error(err);
    });