var fs = require('fs')
var deliveryResponse = fs.readFileSync('public/output/deliveries.json')
var deliveryData = JSON.parse(deliveryResponse)

const deliveryDataForSuperOver = deliveryData.filter(delivery=> delivery.is_super_over === '1');

let bowlerData={}
   deliveryDataForSuperOver.forEach(superOver => {
   const {total_runs,bowler} = superOver
    let runsConceded = parseInt(superOver.total_runs)
    let ballsBowled= 1

    if (bowlerData[bowler]){
        bowlerData[bowler].runsConceded += runsConceded
        bowlerData[bowler].ballsBowled ++
    }
    else{
        bowlerData[bowler] = {runsConceded,ballsBowled}
    }

    
   });
  
Object.keys(bowlerData).forEach(bowler => {

    bowlerData[bowler].oversBowled = Math.floor(bowlerData[bowler].ballsBowled / 6) + ((bowlerData[bowler].ballsBowled % 6) / 10);
    bowlerData[bowler].economyRate = bowlerData[bowler].runsConceded / bowlerData[bowler].oversBowled;

});

let bowlerBestEconomy=[]
for (let [key,value] of Object.entries(bowlerData)){
    bowlerBestEconomy.push([key, value.economyRate])
}

bowlerBestEconomy.sort((a,b)=> a[1] - b[1])
console.log(bowlerBestEconomy)

fs.writeFile("public/output/9-bowler-best-economy-super-over.json",JSON.stringify(bowlerBestEconomy),(err)=>{
if(err){
    console.error(err)
}
else{
    console.log('JSON has been saved')
}

})