var fs = require('fs');
var response = fs.readFileSync('public/output/matches.json')
var data = JSON.parse(response)

let teamWins = {};

data.forEach(match => {
  const {season,winner} = match
  
  if (!teamWins.hasOwnProperty(season)) {
    teamWins[season] = {};
  }
  
  if (!teamWins[season].hasOwnProperty(winner)) {
    teamWins[season][winner] = 1;
   
  } else {
    teamWins[season][winner]++;
  }
});

console.log(teamWins);

fs.writeFile('public/output/2-matches-won-per-team-per-year.json', JSON.stringify(teamWinsPerYear),(err) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log('JSON file has been saved.');
});