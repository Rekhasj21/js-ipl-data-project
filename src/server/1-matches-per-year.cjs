var fs = require('fs')
var words = fs.readFileSync('public/output/matches.json')
var data = JSON.parse(words)

let wins ={}

data.forEach(match => {
  const {season} = match

  if (!wins.hasOwnProperty(season)){
    wins[season] = 1
  }else{
    wins[season] ++
  }
    
});

fs.writeFile('public/output/1-matches-per-year.json', JSON.stringify(wins),(err) => {
    if (err) {
      console.error(err);
      return;
    }
    console.log('JSON file has been saved.');
});