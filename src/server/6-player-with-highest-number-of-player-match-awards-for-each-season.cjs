var fs = require('fs')
var response = fs.readFileSync('public/output/matches.json')
var matchData = JSON.parse(response)

const groupBySeason = matchData.reduce(function(array,match){
    const {season} = match 
    array[season] = array[season] || []
    array[season].push(match)
 return array
},{})

const playerOfMatchBySeason = Object.keys(groupBySeason).reduce((array,season)=>{
    const playerOfMatchCounts = {}
    
    groupBySeason[season].forEach(match => {
        const {player_of_match} = match
        
        playerOfMatchCounts[player_of_match] = playerOfMatchCounts[player_of_match] || 0 
        playerOfMatchCounts[player_of_match] ++
    });
    let max_count = 0 
    let playerOfTheMatch = ''

    Object.keys(playerOfMatchCounts).forEach(player=>{
        if (playerOfMatchCounts[player] > max_count){
            max_count = playerOfMatchCounts[player]
            playerOfTheMatch = player
        }
        else{
            playerOfMatchCounts[player]++
        }
        

    array[season] = { "Player":playerOfTheMatch ,"Awards Won":max_count}

    })
return array
},{})

console.log(playerOfMatchBySeason)

fs.writeFile('public/output/6-player-with-highest-number-of-player-match-awards-for-each-season.json',JSON.stringify(playerOfMatchBySeason),(err)=>{

    if (err){
        console.error(err)
        return 
    }
    console.log('JSON file has been saved')
});