var fs = require('fs');
var responseMatches = fs.readFileSync('public/output/matches.json');
var responseDeliveries = fs.readFileSync('public/output/deliveries.json');

var matchesData = JSON.parse(responseMatches);
var deliveriesData = JSON.parse(responseDeliveries);

const matchesDataForYear2016 = matchesData.filter(match => match.season === '2016')

const matchIdToTeams = {};

matchesDataForYear2016.forEach(match => {
  const {team1 ,team2} = match
  matchIdToTeams[match.id] = [team1,team2]
});

const extraRunsConcededPerTeam={}

deliveriesData.forEach(delivery=>{
  if (delivery.match_id in matchIdToTeams){
    const {bowling_team,extra_runs} = delivery 

    if (bowling_team in extraRunsConcededPerTeam){
      extraRunsConcededPerTeam[bowling_team] += parseInt(extra_runs)
    }
    else{
       extraRunsConcededPerTeam[bowling_team] = parseInt(extra_runs)
    }
  }
})

console.log(extraRunsConcededPerTeam)

fs.writeFile('public/output/3-extra-runs-conceded-per-team-in-2016.json', JSON.stringify(extraRunsConcededPerTeam), (err)=>{
  if (err){
    console.error(err);
    return;
  } 
  console.log("JSON file has been saved")
})