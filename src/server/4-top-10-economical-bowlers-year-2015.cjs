var fs = require('fs')
var matchResponse = fs.readFileSync('public/output/matches.json')
var deliveryResponse = fs.readFileSync('public/output/deliveries.json')

var matchData = JSON.parse(matchResponse)
var deliveryData = JSON.parse(deliveryResponse)

const matchDataForYear2015 = matchData.filter(match => match.season === '2015')

const deliveryDataForYear2015 = deliveryData.filter(delivery=> 
  matchDataForYear2015.some(match=> match.id === delivery.match_id));

const bowlerData = {};
deliveryDataForYear2015.forEach(delivery => {
  const bowler = delivery.bowler;
  const runsConceded = parseInt(delivery.total_runs);
  const ballsBowled = 1;

  if (bowlerData[bowler]) {
    bowlerData[bowler].runsConceded += runsConceded;
    bowlerData[bowler].ballsBowled += ballsBowled;
    
  } else {
    bowlerData[bowler] = { runsConceded, ballsBowled };
  }
});
 
Object.keys(bowlerData).forEach(bowler => {
  bowlerData[bowler].oversBowled = Math.floor(bowlerData[bowler].ballsBowled / 6) + ((bowlerData[bowler].ballsBowled % 6) / 10);
  //bowlerData[bowler].oversBowled = (bowlerData[bowler].ballsBowled / 6).toFixed(3);
  bowlerData[bowler].economyRate = bowlerData[bowler].runsConceded / bowlerData[bowler].oversBowled;
});

let topBowlersData =[]

for (let [key,value] of Object.entries(bowlerData)){
  topBowlersData.push([key,value.economyRate])
}

const top10Bowlers = topBowlersData
                     .sort((a,b)=>a[1] - b[1])
                     .slice(0,10)
  
console.log(top10Bowlers)

fs.writeFile('public/output/4-top-10-economical-bowlers-year-2015.json', JSON.stringify(top10Bowlers), (err)=>{
  if (err){
    console.error(err);
    return;
  }
  else{
     console.log("JSON file has been saved")
  }
})