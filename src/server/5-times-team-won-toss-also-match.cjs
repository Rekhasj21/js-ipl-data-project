var fs = require('fs')
var response = fs.readFileSync('public/output/matches.json')
const data = JSON.parse(response)

let tossMatchWinner = {}

data.forEach(match => {
    const {toss_winner,winner} = match
    
    if (toss_winner === winner) {
        if (tossMatchWinner[toss_winner]) {
            console.log(tossMatchWinner[toss_winner])
            tossMatchWinner[toss_winner]++;
        } 
        else {
            tossMatchWinner[toss_winner] = 1;
        }
    }
});
console.log(tossMatchWinner)

fs.writeFile('public/output/5-times-team-won-toss-also-match.json', JSON.stringify(tossMatchWinner),(err) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log('JSON file has been saved.');
});